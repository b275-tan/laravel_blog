<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// To have access using the authenticated user
use Illuminate\Support\Facades\Auth;

// To have acess with queries related to the Post Entity
use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostComment;

class PostController extends Controller
{
    // action to return a view containing a form for a post creation
    public function create() {
        return view('posts.create');
    }

    // action to receive the form date and subsequently store said data in the post table
    public function store(Request $request){
        // if there is an authenticated user
        if(Auth::user()){
            // instantiate a new post object from the Post model
            $post = new Post;
            // define the properties of the $post object using the received form data
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            // get the id of the authenticated user and set it as the foreign key (user_id)
            $post->user_id = (Auth::user()->id);
            $post->save();

            return redirect('/posts');
        }
        else{
            return redirect('/login');
        }
    }

    // action that will return a view showing all blog posts
    public function index(){
        $posts = Post::all()->where('isActive', '=', 1);
        // The "with()" method will allow us to pass information from the controller to the view page.
        return view('posts.index')->with('posts', $posts);
    }

    // Activity for s02
    public function welcome(){
        $posts = Post::inRandomOrder()->where('isActive', '=', 1)->take(3)->get();
        return view('welcome')->with('posts', $posts);
    }
    

    // action to show only the posts authored by the authenticated user
    public function myPost(){
        if(Auth::user()){
            // We are able to fetch the posts related to a specific user because of the established relationship between models
            $posts = Auth::user()->posts;

            return view('posts.index')->with('posts', $posts);
        }
        else {
            return redirect('/');
        }
    }


    // action that will return a view showing a specific post using the URL parameter $id to query for the database entry to be shown.
    public function show($id){
        $post = Post::find($id);
        return view('posts.show')->with('post', $post);
    }


    // Activity for s03
    // action that will return a view of edit post for the authenticated user of the post
    public function edit($id){
        if(Auth::user()) {
            $post = Post::find($id);

            if(Auth::user()->id == $post->user_id){
                return view('posts.edit')->with('post', $post);
            }
            else {
                return redirect('/posts');
            }
        }
        else {
            return redirect('/login');
        }
    }

    // action to overwrite an existing post when the PUT route with the matching URL parameter ID is accessed.
    public function update(Request $request, $id){
        $post = Post::find($id);
        // if the authenticated user's ID is the same as with the post's user_id.
        if(Auth::user()->id == $post->user_id){
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->save();
        }
        return redirect('/posts');
    }

    // action that will remove a post of the matching URL id parameter
    public function destroy($id){
        $post = Post::find($id);
        if(Auth::user()->id == $post->user_id){
            $post->delete();
        }
        return redirect('/posts');
    }


    // Activity for s04
    // action that will archive a post of the matching URL id parameter
    public function archive($id){
        $post = Post::find($id);

        if(Auth::user()->id == $post->user_id){
            $post->isActive = 0;
            $post->save();
        }
        return redirect('/posts');
    }

    // action that will allow a authenticated user who is not the post author to toggle like/unlike on the post being viewed.
    public function likes($id){
        $post = Post::find($id);

        if(Auth::user()){
            $user_id = Auth::user()->id;

            // check if the authenticated user is not the post owner.
            if($post->user_id != $user_id){

                // check if a post like has been made
                if($post->likes->contains("user_id", $user_id)){
                    // delete the like made by the user to unlike the post
                    PostLike::where('post_id', $post->id)
                        ->where("user_id", $user_id)
                        ->delete();
                }
                else {
                    // create a new like record in the post_likes table with the user_id and post_id
                    $postLike = new PostLike;
                    $postLike->post_id = $post->id;
                    $postLike->user_id = $user_id;
                    $postLike->save();

                }
            }

            return redirect("/posts/$id");
        }
        else{
            return redirect('/login');
        }
    }

    // Activity for s05
    public function comments(Request $request, $id){
        $post = Post::find($id);

        if(Auth::user()){
            $comment = new PostComment;
            $comment->content = $request->input('comment');
            $comment->user_id = Auth::user()->id;
            $comment->post_id = $post->id;
            $comment->save();

            return redirect("/posts/$id");
        }
        else {
            return redirect('/login');
        }
    }
}
