<?php

use Illuminate\Support\Facades\Route;
// link the PostController file
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// defined a rout wherein a view to create a post will be returned to the user.
Route::get('/posts/create', [PostController::class, 'create']);

// define a route wherein form date will be sent via post method to the /posts URI endpoint.
Route::post('/posts', [PostController::class, 'store']);

// define a route that will return a view containing all the posts
Route::get('/posts', [PostController::class, 'index']);

// Activity for s02
Route::get('/', [PostController::class, 'welcome']);

// define a route that will return a view containing only the authenticated user's posts.
Route::get('/myPosts', [PostController::class, 'myPost']);

// define a route wherein a view showing a specific posts with matching URL parameter ID ({}) will be returned to the user.
Route::get('/posts/{id}', [PostController::class, 'show']);

// Activity for s03
    // define a route to show edit form
    Route::get('/posts/{id}/edit', [PostController::class, 'edit']);
    // define a route to update/overwrite the post with marching URL parameter Id using the form via PUT method 
    Route::put('/posts/{id}', [PostController::class, 'update']);


// define a route that will delete a post of the matching URL parameter.
// Route::delete('/posts/{id}', [PostController::class, 'destroy']);


// Activity for s04
// define a route that will archive a post of the matching URL parameter
Route::patch('/posts/{id}', [PostController::class, 'archive']);


// define a route that will call the "like" action when a PUT request is received at the '/posts/{id}/like' endpoint.
Route::put('/posts/{id}/like', [PostController::class, 'likes']);


// Activity for s05
// define a route that will call a modal for placing a comment to the post
Route::post('/posts/{id}/comment', [PostController::class, 'comments']);