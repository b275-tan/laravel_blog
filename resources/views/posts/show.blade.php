@extends('layouts.app')

@section('content')
	<div class="card">
		<div class="card-body">
			<h2 class="card-title">
				{{$post->title}}
			</h2>
			<p class="card-subtitle text-muted">
				Author: {{$post->user->name}}
			</p>
			<p class="card-subtitle text-muted">
				Created at: {{$post->created_at}}
			</p>
			<p class="card-text">
				{{$post->content}}
			</p>
			@if(Auth::user())
				@if(Auth::id() != $post->user_id)
					<form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
						@method('PUT')
						@csrf
						@if($post->likes->contains("user_id", Auth::id()))
							<button type="submit" class="btn btn-danger">Unlike</button>
						@else
							<button type="submit" class="btn btn-success">Like</button>
						@endif	
					</form>
				@endif
				
					<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#commentModal">
						Comment
					</button>
				

					{{-- Modal for the Comment --}}
					<div class="modal fade" id="commentModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
					  <div class="modal-dialog">
					    <div class="modal-content">
					      <div class="modal-header">
					        <h5 class="modal-title" id="exampleModalLabel">Leave a comment:</h5>
					        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					      </div>
					      <div class="modal-body">

					        <form method="POST" action="/posts/{{$post->id}}/comment">
					        	<label for="content">Comment:</label>
					        	<textarea type="text" name="comment" id="comment" class="form-control"></textarea>

					        	@method('POST')
					        	@csrf
					        	<div class="modal-footer">
						        	<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
									<button type="submit" class="btn btn-primary">Comment</button>
					        	</div>
					        </form>

					      </div>
					    </div>
					  </div>
					</div>

			@endif
			<div class="mt-3">
				<a href="/posts" class="card-link">View All Posts</a>
			</div>
		</div>
	</div>

	{{-- Comment Section for the s05 --}}

		<div class="mt-4">
			<h2>Comments:</h2>
		</div>

		@foreach ($post->comments as $comment)
		  <div class="card mb-3">
			<div class="card-body">
			  <h4 class="card-text">{{$comment->content}}</h4>
			  <div class="d-flex">
				<p class="text-muted">Posted by: {{$comment->user->name}}</p>
			  </div>
			  <p class="text-muted">{{$comment->created_at->toDayDateTimeString()}}</p>
			</div>
		  </div>
		@endforeach

	

@endsection